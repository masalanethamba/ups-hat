# UPS  HAT

It’s very common for people to setup Pis as home servers/storage devices/media servers.
With rolling blackouts it can be damaging to the Pi’s SDcard to have the power just cut
randomly and repeatedly. This microHAT will keep your Pi alive for long enough to shut it
down properly (or if your battery is big enough to survive the blackout). To help you decide
when to shut the Pi down it would obviously be helpful to know how full your battery is (how
much power you’ve already used) so the HAT will also have a Voltmeter (or even better
would be a current shunt) that will lead to the signal line between 0-3.3V (In a larger design
this value can be digitised or displayed on an output).
